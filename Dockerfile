FROM tomozkiop/latest:latest

WORKDIR /usr/src/app
RUN apt-get -y update && apt-get -y install git gcc python3-dev ffmpeg

COPY requirements.txt .
RUN pip3 install --no-cache-dir -r requirements.txt

COPY . .

CMD ["bash", "start.sh"]
